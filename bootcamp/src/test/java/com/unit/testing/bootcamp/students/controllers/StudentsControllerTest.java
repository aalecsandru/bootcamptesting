package com.unit.testing.bootcamp.students.controllers;

import com.unit.testing.bootcamp.students.model.Student;
import com.unit.testing.bootcamp.students.services.StudentService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class StudentsControllerTest {

    @Autowired
    StudentsController studentsController;

    @Test
    void testCreateAndDelete() {
        Student student = new Student("Anca", 26, null);

        Student studentResult = studentsController.create(student);

        List<Student> studentList = studentsController.read();
        Assertions.assertThat(studentList).first().hasFieldOrPropertyWithValue("name", "Anca");

        studentsController.delete(studentResult.getId());
        Assertions.assertThat(studentsController.read()).isEmpty();
    }
}