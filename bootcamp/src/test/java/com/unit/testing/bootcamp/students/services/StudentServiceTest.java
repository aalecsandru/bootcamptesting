package com.unit.testing.bootcamp.students.services;

import com.unit.testing.bootcamp.employees.handlers.RecordNotFoundException;
import com.unit.testing.bootcamp.students.dao.StudentRepository;
import com.unit.testing.bootcamp.students.model.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


class StudentServiceTest {

    @InjectMocks
    StudentService service;

    @Mock
    StudentRepository repository;

    public static Student student = null;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @BeforeAll
    static void beforeAll() {
        student = new Student(1, "Anca Test", 26, new ArrayList<>(Arrays.asList(10, 10, 9)));
    }

    @Test
    void shouldFindAllStudents() {
        //given
        Student student = new Student("Anca Alecsandru", 26, new ArrayList<>(Arrays.asList(10, 10, 9)));
        Student studentTwo = new Student("Ion Ionescu", 26, new ArrayList<>(Arrays.asList(7, 7, 7)));

        when(repository.findAll()).thenReturn(Arrays.asList(student, studentTwo));

        //when
        List<Student> studentList = service.findAll();

        //then
        assertNotNull(studentList);
        assertTrue(studentList.size() == 2);
    }


    @Test
    void shouldCreateOrSaveSudent() {
        //given
        student = new Student( "Anca Test", 26, new ArrayList<>(Arrays.asList(10, 10, 9)));

        //when
        service.save(student);

        //then
        verify(repository, times(1)).save(student);
    }

    @Test
    void shouldUpdateStudent() {
        Student existingStudent = new Student( "Anca Test", 26, new ArrayList<>(Arrays.asList(10, 10, 9)));
        //when
        when(repository.findById(any())).thenReturn(Optional.of(existingStudent));


        Student newStudent = new Student( "Anca Alecsandru", 27, new ArrayList<>(Arrays.asList(10, 10, 9)));
        when(repository.save(any())).thenReturn(newStudent);

        Student studentSaved =  service.save(newStudent);

        assertEquals(newStudent.getAge(), studentSaved.getAge());
        assertEquals(newStudent.getName(), studentSaved.getName());
    }

    @Test
    void shouldReturnEmptyListWhenDBisEmpty() {
        //given
        when(repository.findAll()).thenReturn(new ArrayList<>());

        //when
        List<Student> studentList = service.findAll();

        //then
        assertTrue(studentList.isEmpty());
    }


    @Test
    void shouldDeleteStudentById() {
        //given
        when(repository.findById(student.getId())).thenReturn(Optional.of(student));

        //when
        service.deleteById(student.getId());

        //then
        verify(repository, times(1)).deleteById(student.getId());
    }

    @Test
    void shouldThrowRecordNotFoundExceptionWhenStudentIsNotFoundInDB() {
        //given
        when(repository.findById(student.getId())).thenReturn(Optional.ofNullable(null));

        //when-then
        Assertions.assertThrows(RecordNotFoundException.class, () -> {
            service.deleteById(student.getId());
        });
    }
}