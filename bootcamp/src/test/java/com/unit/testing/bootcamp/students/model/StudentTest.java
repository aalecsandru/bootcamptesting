package com.unit.testing.bootcamp.students.model;

import com.unit.testing.bootcamp.students.exceptions.AgeInvalidValueException;
import com.unit.testing.bootcamp.students.exceptions.NoGradesException;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class StudentTest {
    //let's set the test fixtures
    public static final String initialName = "John Doe";
    public static final int initialAge = 20;
    public static final ArrayList<Integer> initialGrades = null;
    public static Student student = null;

    @BeforeAll
    static void beforeAll() {
        System.out.println("Once, Before all tests");

    }

    @AfterAll
    static void afterAll() {
        System.out.println("Once, After all tests");
    }

    @BeforeEach
    void setUp() {
        System.out.println("Before each test");
        student = new Student(initialName, initialAge, initialGrades);
    }

    @AfterEach
    void tearDown() {
        System.out.println("After each test");
    }


    //Right
    @Test
    void testRightSetAge() {
        //given
        Student testStudent = new Student(initialName, initialAge, initialGrades);
        //when
        testStudent.setAge(initialAge);
        //then
        assertEquals(initialAge, testStudent.getAge());
    }


    //Boundary
    @Test
    void testSetAgeBigPositiveValue() {
        Assertions.assertThrows(AgeInvalidValueException.class, () -> {
            student.setAge(130);
        });
    }

    @Test
    void testSetAgeUpperBoundary() {
        student.setAge(Student.MAX_AGE);

        assertEquals(90, student.getAge());
    }

    @Test
    void testSetAgeLowerBoundary() {
        student.setAge(Student.MIN_AGE);

        assertEquals(14, student.getAge());
    }

    //Inverse RelationShip
    @Test
    void testInverseRelationShip() {
        int newAge = initialAge + 1;

        student.setAge(newAge);

        assertNotEquals(initialAge, student.getAge());
    }


    @Test
    void setErrorConditionSetAge() {
        Assertions.assertThrows(AgeInvalidValueException.class, () -> {
            student.setAge(-13);
        });
    }

    //Performance
    @Test
    @Timeout(value = 100, unit = TimeUnit.MILLISECONDS)
    void testTimeOut() {
        student.setAge(initialAge);
        Assertions.assertNotEquals(25, initialAge);
    }

    //Conformance
    //Ordering
    @Test
    void testGetMinGrade() {
        int expectedGrade = 5;

        student.setGrades(new ArrayList<>(Arrays.asList(5, 6, 7, 8, 9, 10)));

        assertEquals(expectedGrade, student.getMinGrade());
    }

    //Range
    @Test
    void testAgeMaxValue() {
        Assertions.assertThrows(AgeInvalidValueException.class, () -> {
            student.setAge(Integer.MAX_VALUE);
        });
    }

    //Reference
    @Test
    void testGetMinGradeReferenceNyll() {
        ArrayList<Integer> grades = null;

        student.setGrades(grades);

        Assertions.assertThrows(NoGradesException.class, () -> {
            student.getMinGrade();
        });
    }

    //Exception
    //Cardinality
    @Test
    void testGetMinGradeCardinalityZero() {
        ArrayList<Integer> grades = new ArrayList<>();

        student.setGrades(grades);

        Assertions.assertThrows(NoGradesException.class, () -> {
            student.getMinGrade();
        });
    }

    //Time


}