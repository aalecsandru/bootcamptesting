package com.unit.testing.bootcamp.students.dao;

import com.unit.testing.bootcamp.students.model.Student;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.*;


@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class StudentRepositoryTest {

    @Autowired
    StudentRepository studentRepository;

    @Test
    void testSaveStudent() {
        Student student = new Student("Anca Alecsandru", 26, new ArrayList<>(Arrays.asList(10, 10, 9)));

        studentRepository.save(student);

        Iterable<Student> studentsList = studentRepository.findAll();

        Assertions.assertThat(studentsList).extracting(Student::getName).contains("Anca Alecsandru");
    }

    @Test
    void shouldDeleteAllStudents() {
        Student student = new Student("Anca Alecsandru", 26, new ArrayList<>(Arrays.asList(10, 10, 9)));
        Student studentTwo = new Student("Ion Ionescu", 26, new ArrayList<>(Arrays.asList(7, 7, 7)));

        studentRepository.save(student);
        studentRepository.save(studentTwo);
        List<Student> studentsSaved = studentRepository.findAll();
        Assertions.assertThat(studentsSaved.contains(studentTwo));

        studentRepository.deleteAll();
        List<Student> afterDeleteAllList = studentRepository.findAll();
        Assertions.assertThat(afterDeleteAllList.isEmpty());
    }
}
