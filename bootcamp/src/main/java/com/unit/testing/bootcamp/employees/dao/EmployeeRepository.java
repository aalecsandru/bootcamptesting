package com.unit.testing.bootcamp.employees.dao;

import com.unit.testing.bootcamp.employees.model.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Integer> {

}
