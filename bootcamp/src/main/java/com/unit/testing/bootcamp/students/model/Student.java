package com.unit.testing.bootcamp.students.model;

import com.unit.testing.bootcamp.students.exceptions.AgeInvalidValueException;
import com.unit.testing.bootcamp.students.exceptions.NoGradesException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.Collections;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    String name;
    int age;
    ArrayList<Integer> grades;

    public static final int MIN_AGE = 14;
    public static final int MAX_AGE = 90;


    public Student(String name, int age, ArrayList<Integer> grades) {
        super();
        this.setName(name);
        this.setAge(age);
        this.setGrades(grades);
    }


    public void setAge(int age) {
        if(age >= MIN_AGE && age <= MAX_AGE) {
            this.age = age;
        }
        else {
            throw new AgeInvalidValueException();
        }
    }

    public ArrayList<Integer> getGrades() {
        return grades;
    }

    public void setGrades(ArrayList<Integer> grades) {
        this.grades = grades;
    }

    public int getMinGrade() {
        if(this.grades == null || this.grades.size() == 0)
            throw new NoGradesException();

        return  Collections.min(grades);
    }

    /**
     * TODO
     * 1. implement method
     * 2. unit test it according to R-BICEP and CORRECT guidelines
     * @return
     */
    public Double getGradesAverage() {

        return 0.0;
    }
}
