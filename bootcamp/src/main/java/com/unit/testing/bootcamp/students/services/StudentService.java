package com.unit.testing.bootcamp.students.services;

import com.unit.testing.bootcamp.employees.handlers.RecordNotFoundException;
import com.unit.testing.bootcamp.students.dao.StudentRepository;
import com.unit.testing.bootcamp.students.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    @Autowired
    StudentRepository studentRepository;

    public Student save(Student student) {

        if (student.getId() == null) {
            student = studentRepository.save(student);
            return student;
        } else {
            Optional<Student> studentOptional = studentRepository.findById(student.getId());
            if (studentOptional.isPresent()) {
                Student newEntity = studentOptional.get();
                newEntity.setName(student.getName());
                newEntity.setAge(student.getAge());
                newEntity.setGrades(student.getGrades());

                newEntity = studentRepository.save(newEntity);

                return newEntity;
            } else {
                throw new RecordNotFoundException("No student record exist for given id");
            }
        }
    }


    public List<Student> findAll() {
        List<Student> result = (List<Student>) studentRepository.findAll();

        if (result.size() > 0) {
            return result;
        } else {
            return new ArrayList<>();
        }
    }

    public void deleteById(Integer id) {
        Optional<Student> student = studentRepository.findById(id);

        if (student.isPresent()) {
            studentRepository.deleteById(id);
        } else {
            throw new RecordNotFoundException("No student record exist for given id");
        }
    }

    void deleteAll() {
        studentRepository.deleteAll();
    }
}
