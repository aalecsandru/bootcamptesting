package com.unit.testing.bootcamp.students.dao;

import com.unit.testing.bootcamp.students.model.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends CrudRepository<Student, Integer> {
    @Override
    List<Student> findAll();
}
