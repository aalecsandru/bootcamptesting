package com.unit.testing.bootcamp.students.controllers;

import com.unit.testing.bootcamp.students.model.Student;
import com.unit.testing.bootcamp.students.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class StudentsController {
    @Autowired
    StudentService studentService;

    @PostMapping("/student")
    Student create(@RequestBody Student student)  {
        return studentService.save(student);
    }

    @GetMapping("/student")
    List<Student> read() {
        return studentService.findAll();
    }

    @PutMapping("/student")
    Student update(@RequestBody Student Student) {
        return studentService.save(Student);
    }

    @DeleteMapping("/student/{id}")
    void delete(@PathVariable Integer id) {
        studentService.deleteById(id);
    }

}
